package reversi;

import java.io.BufferedInputStream;
import java.io.IOException;

/**
 * Trida pro cteni dat prichazejicich do socketu. 
 * Cteni probiha ve smycce ve samostatnem vlakne.
 * 
 * @author Michal Zak
 *
 */
public class TcpReader extends Thread {
	
	private Game game;
	private BufferedInputStream reader;
	
	
	/**
	 * Konstruktor cteciho vlakna.
	 * @param game reference na tridu herni logiky
	 */
	public TcpReader(Game game) {
		this.game = game;
	}
	
	/**
	 * Pocatek behu vlakna obstaravajiciho cteni zprav z portu.
	 * @param clientSocket socket
	 * @param reader strean, do ktereho chceme cist data
	 */
	synchronized public void startReading(BufferedInputStream reader) {		
		this.reader = reader;
		try {
			this.start();
		} 
		catch (IllegalThreadStateException e) {
			/* vlakno jiz bezelo. */
		}
		
	}
	
	/**
	 * Behova metoda tridy, obsahuje smycku pro cteni ze socketu.
	 */
	public void run() {
		int message;
		while (true) {
			
			try {
								
				message = reader.read(); // hlavicka zpravy							
				
				if (game == null)
					return;						
				
				game.reversi.tcp.log("Prijata zprava, dekoduji hlavicku...", 0, 1);
				
				// podle typu zpravy se nasledne provadi ruzne akce
				switch (message) {
				case TcpClient.MESSAGE_NOOP:
					game.reversi.tcp.log("Prijato NOOP (keep alive).", 0, 0);
					break;
				case TcpClient.MESSAGE_TEXT:
					game.reversi.tcp.log("Prijato TEXT (textova zprava).", 0, 0);
					processText();
					break;
				case TcpClient.MESSAGE_GAME_STATE:
					game.reversi.tcp.log("Prijato GAMESTATE (zprava se stavem hry).", 0, 0);
					if (!processGameState())
						return;
					else
						break;
				case -1:
					game.reversi.tcp.log("Prijata zprava byla detekovana jako chybovy stav.", 0, 0);
					game.reversi.getTcp().disconnect();
					game.log("Připojení k serveru bylo ztraceno", true);
					game.reversi.enableConnectPanel();
					return;
				default:
					game.reversi.tcp.log("Prijata neznama zprava.", 0, 0);
				}
				
				
			} catch (IOException e) {
				if (game != null && game.reversi != null && game.reversi.getTcp() != null) {
					
					if (!game.reversi.getTcp().isConnected())
						return; // odpojeno - skonci
					else
					{
						game.reversi.tcp.log("Prijata zprava zpusobila vyjimku.", 0, 0);
						game.reversi.getTcp().disconnect();						
						game.log("Připojení k serveru bylo ztraceno", true);
						game.reversi.enableConnectPanel();
					}
					
				} else return; // neplatne reference - skonci
			}
			
		}//^ while true ^
	}
	
	/**
	 * Prijem textove zpravy.
	 * @throws IOException vyjimka je vyhozena v pripade, ze se nezadarilo cteni ze streamu, predpoklada
	 *                     se reseni ve volajici metode
	 */
	private void processText() throws IOException {
		int temp;
		StringBuffer msg = new StringBuffer();
		
		while ((temp = reader.read()) > 0)
			msg.append((char)temp);
				
		if (temp == -1)
			throw new IOException("Prijem zpravy se nezdaril.");
			
		String msgString = msg.toString();
		int msgLength = msgString.length() + 1; // +1 znamena ukoncovaci znak ('\0')
		
		game.log(msgString, true);
		game.reversi.tcp.log("Textova zprava: " + msgString + "(" + msgLength + " bajtu)", 0, msgLength);		
	}
	
	/**
	 * Zpracovani aktualniho herniho stavu.
	 * @throws IOException vyjimka je vyhozena v pripade, ze se nezadarilo cteni ze streamu, predpoklada
	 *                     se reseni ve volajici metode
	 */
	private boolean processGameState() throws IOException {
		
		game.reversi.tcp.log("Dekodovani GAMESTATE.", 0, 0);
		
		// id hry
		int gameId = (reader.read() << 24);
		gameId += (reader.read() << 16);
		gameId += (reader.read() << 8);
		gameId += (reader.read() << 0);
		
		game.reversi.tcp.log("GAMESTATE - id hry: " + gameId +".", 0, 4);
		
		// id hrace
		int playerId = reader.read();
		
		game.reversi.tcp.log("GAMESTATE - id hrace: " + playerId +".", 0, 1);
		
		// cislo tahu
		int turns = (reader.read() << 24);
		turns += (reader.read() << 16);
		turns += (reader.read() << 8);
		turns += (reader.read() << 0);
		
		game.reversi.tcp.log("GAMESTATE - tah: " + turns +".", 0, 4);
		
		// stav hry
		int state = reader.read();
		
		game.reversi.tcp.log("GAMESTATE - stav hry: " + state +".", 0, 1);
		
		// hrac na tahu
		int playerOnTurn = reader.read();								
		// kdo vyhral
		reader.read();
		
		game.reversi.tcp.log("GAMESTATE - prijaty dodatecne informace.", 0, 2);
		
		// herni pole
		int[] fields = new int[64];
		for (int i = 0; i < 64; i++)
		  fields[i] = reader.read();
		
		game.reversi.tcp.log("GAMESTATE - prijata sachovnice (herni plocha).", 0, 64);
		
		if (state == 0)
		{
			if (playerOnTurn == playerId)
				game.log("Jste na tahu.", true);
			else
				game.log("Na tahu je soupeř.", true);
		}
		
		game.setAll(gameId, playerId, turns, fields, state);
		
		game.reversi.tcp.log("GAMESTATE - vse uspesne dekodovano.", 0, 0);
		
		return (state == 0 || state == 2) ? true : false;
	}
}
