package reversi;

import java.io.File;
import java.io.PrintStream;
import java.util.Scanner;

/**
 * Herni trida - stara se o herni logiku, slouzi jako vrstva mezi sitovym a grafickym rozhranim.
 * 
 * @author Michal Zak
 *
 */
public class Game {
	
	/**
	 * reference na gui.
	 */
	Reversi reversi;
	
	/**
	 * Herni pole.
	 */
	private int[] fields;
	private int gameId;
	private int playerId;	
	@SuppressWarnings("unused")
	private int turns;
	
	public boolean flagReceivedData;
	
	final static int WHITE = 1;
	final static int BLACK = 2;
  
	/**
	 * Konstruktor tridy.
	 * @param reversi reference na grafickou tridu
	 */
	public Game(Reversi reversi) {
		this.reversi = reversi;
		fields = new int[8 * 8];	
	}
	
	/**
	 * Zalozeni nove hry, opticke vyplneni poli.
	 */
	public void newGame() {
		for (int j = 0; j < 8; j++)
		for (int i = 0; i < 8; i++)
			fields[j * 8 + i] = 0;
		
		placePiece(WHITE, 3, 3);
		placePiece(WHITE, 4, 4);
		placePiece(BLACK, 3, 4);
		placePiece(BLACK, 4, 3);
	}
	
	/**
	 * Provedeni tahu na danou pozici.
	 * @param x x
	 * @param y y
	 */
	public void turn(int x, int y) {
		reversi.tcp.sendTurn(x, y);
	}
	
	/**
	 * Logovani zpravy. Dochazi k preposlani do (vlastnici) tridy Reversi.
	 * @param message zprava urcena k zapisu
	 * @param show urcuje, jestli se ma vypsat i na obrazovku
	 */
	public void log(String message, boolean showinGUI) {
		if (reversi != null)
			reversi.log(message, showinGUI);
	}
	
	
	/**
	 * Nastaveni vsech dulezitych stavu.
	 * @param gameId
	 * @param playerId
	 * @param turns
	 * @param fields
	 * @param state
	 */
	public void setAll(final int gameId, final int playerId, final int turns, final int[] fields, int state) {
		if (playerId >= 1 && playerId <= 2)
			this.playerId = playerId;
		
		this.gameId = gameId;
		this.turns = turns;
		
		for (int y = 0; y < 8; y++)
			for (int x = 0; x < 8; x++)
				if (fields[y * 8 + x] != this.fields[y * 8 + x])
					placePiece(fields[y * 8 + x], x, y);
		
		switch (state)
		{
		case 1:
			reversi.tcp.disconnect();			
			log("Hra končí.", true);
			this.gameId = -1;
			reversi.enableConnectPanel();
			break;
		case 2:			
			log("Spojení s druhý hráčem bylo ztraceno.", true);
			log("Prosím počkejte na jeho opětovné připojení.", true);			
			break;
		default:				
		}
		
		saveGameId();
		flagReceivedData = true;
	}
	
	// - - - private - - -
	
	/**
	 * Umisteni kamene bez kontroly serverem, bez posilani zpravy.
	 * Slouzi pro vnitrni graficke ucely.
	 */
	private void placePiece(int player, int x, int y) {
		fields[y * 8 + x] = player;
		reversi.displayPiece(player, x, y);
	}
	
	/**
	 * Ulozeni identifikacniho cisla hry pro opetovne pripojeni.
	 */
	private void saveGameId()
	{		
		try {
			PrintStream ps = new PrintStream(new File("lastgameid.dat"));
			ps.println(gameId);
			ps.println(playerId);
			ps.close();
		} catch (Exception e) {
			/* toto nam zdaleka nevadi, zkratka se to neulozi (hrac se holt nepripoji zpatky do hry) */
		}
	}
	
	/**
	 * Nacteni id hry pro opetovne pripojeni.
	 * @return pole obsahujici id hry a id hrace
	 */
	public static int[] loadGameId()
	{
		int[] rtn = new int[2];
		try {
			Scanner sc = new Scanner(new File("lastgameid.dat"));
			rtn[0] = sc.nextInt();
			rtn[1] = sc.nextInt();
			sc.close();
		} catch (Exception e) {			
			rtn[0] = -1;
			rtn[1] = 0;
		}
		return rtn;
	}
  
}
