package reversi;

import java.io.*;
import java.net.*;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Trida pro udrzeni pripojeni a zasilani zprav. Dale spravuje objekt prijemce zprav, sitovy log a statistiky.
 * 
 * @author Michal Zak
 *
 */
public class TcpClient {
		
	private Socket clientSocket;
	private DataOutputStream outToServer;
	private BufferedInputStream inFromServer;
	private TcpReader asyncReader;
	
	private String lastError;
	private boolean connected;
	
	private Game game;
	
	private int bytesSent;
	private int bytesReceived;
	
	/**
	 * Timer pro udrzovani spojeni.
	 */
	private Timer keepAliveTimer;
	
	/* typy zprav */
	
	/**
	 * Zprava "No operation". Muze byt vyuzita pro detekci padu spojeni.
	 */
	public static final byte MESSAGE_NOOP = 0;
	/**
	 * Zprava "Text". Prenasi retezec zakonceny znakem '\0'. Obsahuje libovolne informativni zpravy.
	 */
	public static final byte MESSAGE_TEXT = 1;
	/**
	 * Zprava "Herni stav". Obsahuje kompletni popis herni plochy.
	 */
	public static final byte MESSAGE_GAME_STATE = 2;
	/**
	 * Zprava "Tah". Rika, na ktere policko chce hrac polozit kamen.
	 */
	public static final byte MESSAGE_TURN = 3; 
	
	/**
	 * Frekvence zasilani NOOP signalu.
	 */
	private static final int KEEPALIVE_INTERVAL = 10000;
	
	public TcpClient(Game game) {
		this.game = game;		
		connected = false;
		
		bytesSent = 0;
		bytesReceived = 0;
	}

	/**
	 * Pripojeni k hernimu serveru.
	 * @param address ip adresa
	 * @param port cilovy port
	 * @param reconnectToGame id hry, kam se chceme opetovne pripojit, 0 pro normalni matchmaking
	 * @return true, pokud operace probehla v poradku - pripadna chyba je v lastError
	 */
	public boolean connect(String address, String port, int reconnectToGame, int reconnectAs,
			boolean reconnectToGameOn) {
		try
		{		
			clientSocket = new Socket();
                        clientSocket.connect(new InetSocketAddress(address, Integer.parseInt(port)), 5000);
			outToServer = new DataOutputStream(new BufferedOutputStream(clientSocket.getOutputStream()));
			inFromServer = new BufferedInputStream(clientSocket.getInputStream());					
		} 
		catch (UnknownHostException e )
		{
			lastError = e.getLocalizedMessage();
			try { 
				clientSocket.close(); 
			} 
			catch (Exception e1) { 
				/* nechceme uz nic resit */
			}
			return false;
		}
		catch (IOException e)
		{
			lastError = e.getLocalizedMessage();
			try { 
				clientSocket.close(); 
			} 
			catch (Exception e1) { 
				/* nechceme uz nic resit */
			}
			return false;
		}
		
		game.flagReceivedData = false;
		
		asyncReader = new TcpReader(game);
		asyncReader.startReading(inFromServer);
		
		if (reconnectToGameOn)
		{
			log("Hrac obnovuje pripojeni k serveru...", 0, 0);
			log("Zasilam id hry a id hrace...", 0, 0);
			try {
				outToServer.writeInt(reconnectToGame);
				outToServer.writeInt(reconnectAs);
				outToServer.flush();
			} catch (IOException e) {
				log("Doslo k chybe pri zasilani id hry a id hrace.", 0, 0);
				return false;
			}
			log("Id hry a id hrace bylo uspesne odeslano.", 8, 0);
		}
		
		
		game.log("Připojeno, čekám na samotné připojení do hry...", true);
		
		keepAliveTimer = new Timer();
		keepAliveTimer.scheduleAtFixedRate(new TimerTask() {

			@Override
			public void run() {
				if (game.flagReceivedData)
					sendNOOP();
			}
			
		}, KEEPALIVE_INTERVAL, KEEPALIVE_INTERVAL);
		
		connected = true;
		log("Hrac se pripojil k serveru.", 0, 0);
		return true;
	}
	
	/**
	 * Odpojeni od serveru.
	 * @return true, pokud operace probehla v poradku - pripadna chyba je v lastError
	 */
	public boolean disconnect() {
		connected = false;
		outToServer = null;
		inFromServer = null;		
		
		keepAliveTimer.cancel();
		
		try {
			clientSocket.close();
		}
		catch (IOException e) {
			lastError = e.getLocalizedMessage();
			clientSocket = null;
			return false;
		}
		clientSocket = null;		
		
		log("Hrac se odpojil od serveru.", 0, 0);
		
		log("Statistiky - odeslano: " + bytesSent + ", prijato: " + bytesReceived, 0, 0);
		bytesSent = 0;
		bytesReceived = 0;
		
		return true;
	}
	
	/**
	 * Posle pozadavek serveru - chci tahnout na pole x, y.
	 * @param x souradnice X
	 * @param y souradnice Y
	 */
	public void sendTurn(final int x, final int y) {
		
		if (!connected)
			return;		
		
		log("Posilam signal TURN (3 bajty)...", 0, 0);
		
		try {
			outToServer.writeByte(MESSAGE_TURN);
			outToServer.writeByte((byte)x);
			outToServer.writeByte((byte)y);
			outToServer.flush();
		} catch (IOException e) { 
			// OSETRENI CHYBY
			e.printStackTrace();
		}
		
		log("Signal TURN byl uspesne zaslan.", 3, 0);
	}
	
	/**
	 * Zaslani NOOP pro udrzeni spojeni.
	 */
	public void sendNOOP() {
		
		if (!connected)
			return;
		
		log("Posilam signal NOOP (1 bajt)...", 0, 0);
		bytesSent += 1;
		
		try {
			outToServer.writeByte(MESSAGE_NOOP);
			outToServer.flush();
		} catch (IOException e) { 
			// OSETRENI CHYBY
			e.printStackTrace();
			return;
		}
		
		log("Signal NOOP byl uspesne zaslan.", 1, 0);
	}
	
	/**
	 * Getter zjistujici, je-li client pripojen k serveru.
	 * @return true, pokud je pripojen
	 */
	public boolean isConnected() {
		return connected;
	}
	
	/**
	 * Ziskani textu poslechni vznikle chyby.
	 * @return posledni vznikla chyba
	 */
	public String getLastError() {		
		return lastError;
	}	
	
	/**
	 * Cekani na dokonceni cteciho vlakna.
	 */
	public void waitForReaderExit() {
		
		log("Cekam na ukonceni cteciho vlakna...", 0, 0);
		
		if (asyncReader != null && asyncReader.isAlive()) {
			try {
				asyncReader.join();
			} catch (InterruptedException e) {
				/* */
			}
	    }
	}
	
	/**
	 * Zapis informaci do logu, zapocteni bajtu do statistik.
	 * @param message zprava
	 * @param bytesSent odeslane bajty
	 * @param bytesReceived prijate bajty
	 */
	synchronized public void log(String message, int bytesSent, int bytesReceived) {
		
		this.bytesSent += bytesSent;
		this.bytesReceived += bytesReceived;
		
		FileWriter f;
		try {
			f = new FileWriter("tcp_msg.log", true);
		} catch (IOException e) {
			return;
		}
		
		PrintWriter p;
		p = new PrintWriter(f); 
		
		p.println(message);
		p.close();
	}
}
