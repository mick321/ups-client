package reversi;
 
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.*;

/**
 * Trida pro zobrazovani stavu hry. Pouziva Swing pro kresleni komponent.
 * 
 * @author Michal Zak
 *
 */
public class Reversi implements ActionListener {	
	
	private JFrame appWindow;
	private JPanel panelGame, panelConnection, panelMessages;
	private JTextField textIp, textPort;
	private JButton btnConnect, btnReconnect;
	private JTextArea memoLog;
	private JScrollPane memoscroll;	
	
	final private static int APP_WINDOW_SIZE_W = 420;
	final private static int APP_WINDOW_SIZE_H = 640;
	final private static int PANEL_WIDTH = 380;
	final private static String APP_NAME = "Reversi";
	
	final private static Color[] PLAYER_COLOR = { Color.getHSBColor(0.35f, 0.4f, 0.65f), Color.WHITE, Color.BLACK };
	
	JButton[] buttons;	
	
	/**
	 * Trida pro pripojeni k serveru a vymenu informaci s nim.
	 */
	TcpClient tcp;
	/**
	 * Trida hernich pravidel.
	 */
	Game game;
	
	/**
	 * Inicializace aplikacni tridy.
	 * @return vraci true, pokud se vse zadarilo
	 */
	private boolean run() {		
		game = new Game(this);
		tcp = new TcpClient(game);
		prepareComponents();		
		game.newGame();
		return true;		
	}
	
	/**
	 * Pripraveni komponent aplikace.
	 */
	private void prepareComponents() {
		appWindow = new JFrame();		
		appWindow.setSize(APP_WINDOW_SIZE_W, APP_WINDOW_SIZE_H);
		appWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		appWindow.setTitle(APP_NAME);
		appWindow.setLocationByPlatform(true);
		appWindow.setResizable(false);
		appWindow.setLayout(new FlowLayout());
		
		panelConnection = new JPanel();
		panelConnection.setPreferredSize(new Dimension(PANEL_WIDTH, 80));
		panelConnection.setLayout(new FlowLayout());		
		
		JLabel labelIp = new JLabel();		
		labelIp.setText("IP adresa serveru");
		panelConnection.add(labelIp);
		
		textIp = new JTextField();
		textIp.setText("192.168.56.101");
		textIp.setPreferredSize(new Dimension(100, 32));
		panelConnection.add(textIp);
		
		JLabel labelPort = new JLabel();			
		labelPort.setText("Port");
		panelConnection.add(labelPort);
		
		textPort = new JTextField();
		textPort.setText("8000");
		textPort.setPreferredSize(new Dimension(40, 32));
		panelConnection.add(textPort);
		
		btnConnect = new JButton();
		btnConnect.setText("Připojit");
		btnConnect.setPreferredSize(new Dimension(80, 32));
		btnConnect.setMargin(new Insets(3, 3, 3, 3));
		btnConnect.addActionListener(this);
		btnConnect.setActionCommand("CONNECT");
		
		panelConnection.add(btnConnect);
		
		btnReconnect = new JButton();
		btnReconnect.setText("Obnovení připojení");
		btnReconnect.setPreferredSize(new Dimension(200, 32));
		btnReconnect.setMargin(new Insets(3, 3, 3, 3));
		btnReconnect.addActionListener(this);
		btnReconnect.setActionCommand("RECONNECT");
				
		panelConnection.add(btnReconnect);
		
		appWindow.add(panelConnection);
		
		panelMessages = new JPanel(); 
		memoLog = new JTextArea();		
		memoLog.setPreferredSize(new Dimension(PANEL_WIDTH - 100, 100));
		memoLog.setEditable(false);
		
		memoscroll = new JScrollPane(memoLog);
		memoscroll.setPreferredSize(new Dimension(PANEL_WIDTH, 100));
		memoscroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		
		panelMessages.add(memoscroll);
		
		appWindow.add(panelMessages);
		
		// herni pole
		panelGame = new JPanel();
		panelGame.setBackground(Color.WHITE);
		panelGame.setPreferredSize(new Dimension(PANEL_WIDTH, 390));
		panelGame.setLayout(null);
		
		buttons = new JButton[8 * 8];
		for (int j = 0; j < 8; j++)
			for (int i = 0; i < 8; i++)
			{
				int index = j * 8 + i;				
				buttons[index] = new JButton();
				buttons[index].setLocation(10 + i * (PANEL_WIDTH-15)/8, 10 + j * (PANEL_WIDTH-15)/8);
				buttons[index].setSize((PANEL_WIDTH - 60)/8, (PANEL_WIDTH - 60)/8);
				buttons[index].setBackground(PLAYER_COLOR[0]);				
				buttons[index].addActionListener(this);			
				buttons[index].setActionCommand(Integer.toString(index));
				buttons[index].setMargin(new Insets(3, 3, 3, 3));
				panelGame.add(buttons[index]);
			}
		
		appWindow.add(panelGame);		
		appWindow.addWindowListener(new CloseListener(this));
		
		appWindow.setVisible(true);
		
		
		
		log("Vítejte ve hře Reversi určené pro více hráčů.", true);
	}
	
	/**
	 * Metoda pro obarveni policka podle prislusneho kamenu.
	 * @param player hrac - 0 pro zadny kamen, 1 - prvni hrac, 2 - druhy hrac
	 * @param x souradnice x
	 * @param y souradnice y
	 */
	synchronized public void displayPiece(final int player, final int x, final int y) {
		if (player < 0 || player > 2 || x < 0 || x >= 8 || y < 0 || y >= 8)
			return;
		
		final int index = y * 8 + x;
		
		
		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() { 
				buttons[index].setBackground(PLAYER_COLOR[player]); 
			}
			
		});
	}
	
	/**
	 * Vstupni metoda programu.
	 * @param args
	 */
	public static void main(String[] args) {
		Reversi reversi = new Reversi();
		if (!reversi.run())
		{
			System.err.println("Doslo k chybe pri vytvareni okna a aplikace bude ukoncena.");			
		}		
	}
	
	/**
	 * Opetovne odemceni panelu k pripojeni k serveru.
	 */
	synchronized public void enableConnectPanel() {
		btnConnect.setEnabled(true);
		btnReconnect.setEnabled(true);
		btnConnect.setText("Připojit.");
		textIp.setEnabled(true);
		textPort.setEnabled(true);		
	}

	/**
	 * Sledovani udalosti.
	 */
	@Override
	synchronized public void actionPerformed(ActionEvent event) {
		
		if (event.getActionCommand().equals("CONNECT")) {
			if (tcp.isConnected())
				tcp.disconnect();
			if (!tcp.connect(textIp.getText(), textPort.getText(), 0, 0, false)) {				
				log(tcp.getLastError(), true);
			}
			else {	
				btnConnect.setEnabled(false);
				btnReconnect.setEnabled(false);
				btnConnect.setText("Připojeno.");
				textIp.setEnabled(false);
				textPort.setEnabled(false);
			}
		}
		else if (event.getActionCommand().equals("RECONNECT")) {
			if (tcp.isConnected())
				tcp.disconnect();
			
			int[] lastGameIdAndPlayer = Game.loadGameId();
			if (lastGameIdAndPlayer[0] == -1)
			{
				log("Hru nelze obnovit!", true);
			}				
			else if (!tcp.connect(textIp.getText(), textPort.getText(), 
					lastGameIdAndPlayer[0],
					lastGameIdAndPlayer[1], true)) {				
				log(tcp.getLastError(), true);
			}
			else {	
				btnConnect.setEnabled(false);
				btnReconnect.setEnabled(false);
				btnConnect.setText("Připojeno.");
				textIp.setEnabled(false);
				textPort.setEnabled(false);
			}
		}
		else
		{
			JButton button = (JButton)event.getSource();
			if (button != null)
			{
				String command = event.getActionCommand();
				int coord = Integer.parseInt(command);
				if (game != null && tcp.isConnected())
					game.turn(coord % 8, coord / 8);
			}
		}
	}
	
	/**
	 * Logovani zpravy.
	 * @param message zprava urcena k zapisu
	 * @param show urcuje, jestli se ma vypsat i na obrazovku
	 */
	synchronized public void log(final String message, boolean showinGUI) {
		if (showinGUI) {	
			
			SwingUtilities.invokeLater(new Runnable() {

				@Override
				public void run() { 
					memoLog.append(message + "\n");
					memoLog.setPreferredSize(new Dimension(PANEL_WIDTH - 100, memoLog.getLineCount() * 16));					
					memoscroll.scrollRectToVisible(memoLog.getVisibleRect());
					memoLog.setCaretPosition(memoLog.getText().length());
				}
				
			});
			
		}
	}
	
	/**
	 * Getter pro vlakno obsluhujici tcp clienta.
	 * @return reference na tcp clienta
	 */
	public TcpClient getTcp() {
		return tcp;
	}
	
	
	/**
	 * Obsluha zavreni aplikace a odpojeni od serveru.
	 */
	private class CloseListener extends WindowAdapter {
		
		Reversi reversi;
		
		public CloseListener(Reversi owner) {
			reversi = owner;
		}
		
		@Override
		public void windowClosing(WindowEvent e) {
			if (reversi.getTcp().isConnected())
				if (!reversi.getTcp().disconnect())
				{
					reversi.log(reversi.getTcp().getLastError(), true);
				}
			tcp.waitForReaderExit();
		}
		
		
	}

}
